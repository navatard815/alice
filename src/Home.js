import { useState } from "react";

export default function Home({ handleSubmit }) {
  const [pseudonym, setPseudonym] = useState("");

  const onSubmit = (event) => {
    event.preventDefault();
    handleSubmit(pseudonym);
  };

  return (
    <form onSubmit={onSubmit}>
      <label>
        Pseudonym:
        <input
          type="text"
          value={pseudonym}
          onChange={(e) => setPseudonym(e.target.value)}
        />
      </label>
      <button type="submit">Create Lobby</button>
    </form>
  );
}
