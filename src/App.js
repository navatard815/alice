import React, { useState } from "react";
import { Route, useNavigate, Routes } from "react-router-dom";
import Home from "./Home";
import Lobby from "./Lobby";
import JoinLobby from "./JoinLobby";

function App() {
  const [lobbyLink, setLobbyLink] = useState("");
  const [lobbyId, setLobbyId] = useState("");
  const [pseudonym, setPseudonym] = useState("");
  const navigate = useNavigate();

  const handleSubmit = async (pseudonym) => {
    // Make a request to your backend to create a new lobby
    // with the pseudonym as a parameter
    const response = await fetch(`http://localhost:3080/api/lobbies`, {
      method: "POST",
      body: JSON.stringify({ pseudonym }),
      headers: { "Content-Type": "application/json" },
    });
    const { lobbyId, lobbyLink } = await response.json();
    setLobbyId(lobbyId);
    setLobbyLink(lobbyLink);
    setPseudonym(pseudonym);
    navigate(`/lobby/${lobbyId}`);
  };

  return (
    <Routes>
      <Route
        path="/"
        element={
          <Home handleSubmit={handleSubmit} setPseudonym={setPseudonym} />
        }
      />
      <Route
        path="/lobby/:lobbyId"
        element={
          <Lobby
            lobbyId={lobbyId}
            lobbyLink={lobbyLink}
            pseudonym={pseudonym}
          />
        }
      />
      <Route path="/lobby/:lobbyId/join" element={<JoinLobby />} />
    </Routes>
  );
}

export default App;
