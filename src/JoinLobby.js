import { useState, useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom";

export default function JoinLobby() {
  const [pseudonym, setPseudonym] = useState("");
  const navigate = useNavigate();
  const { lobbyId } = useParams();

  useEffect(() => {
    async function verifyLobbyId() {
      const response = await fetch(
        `http://localhost:3080/api/lobbies/${lobbyId}`
      );
      if (response.status === 200) {
        // lobbyId is valid
      } else {
        // lobbyId is not valid, redirect to home page
        navigate("/");
      }
    }

    verifyLobbyId();
  }, [lobbyId, navigate]);

  const handleSubmit = async (event) => {
    event.preventDefault();
    const response = await fetch(
      `http://localhost:3080/api/lobbies/${lobbyId}/join`,
      {
        method: "POST",
        body: JSON.stringify({ pseudonym }),
        headers: { "Content-Type": "application/json" },
      }
    );
    navigate(`/lobby/${lobbyId}`);
  };

  return (
    <form onSubmit={handleSubmit}>
      <label>
        Pseudonym:
        <input
          type="text"
          value={pseudonym}
          onChange={(e) => setPseudonym(e.target.value)}
        />
      </label>
      <button type="submit">Join Lobby</button>
    </form>
  );
}
