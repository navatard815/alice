import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { useParams } from "react-router-dom";

export default function Lobby({ lobbyId, lobbyLink, pseudonym }) {
  const lobbyJoinLink = lobbyLink + "/join";
  const [players, setPlayers] = useState("");

  //const navigate = useNavigate();

  useEffect(() => {
    async function showPlayers() {
      const response = await fetch(
        `http://localhost:3080/api/lobbies/${lobbyId}/players`
      );
      if (response.status === 200) {
        const players = await response.json();
        // display the players in the lobby page
        setPlayers(players);
      } else {
        // handle error
      }
    }
    showPlayers();
  });

  /*useEffect(() => {
    if (!pseudonym) {
      navigate("/lobby/:lobbyId/join");
    }
  }, [pseudonym, navigate]);*/
  return (
    <div>
      <p>Your lobby ID is: {lobbyId}</p>
      <p>Your pseudonym is : {pseudonym}</p>
      <button onClick={() => navigator.clipboard.writeText(lobbyJoinLink)}>
        Copy lobby link
      </button>
      <ul>
        {players &&
          players.map((player) => {
            return <li>{player}</li>;
          })}
      </ul>
    </div>
  );
}
