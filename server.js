const express = require("express");
const mongodb = require("mongodb");
const cors = require("cors");
const bodyParser = require("body-parser");

const app = express();
const port = 3080;

app.use(cors());
app.listen(port, () => {
  console.log(`Server started on http://localhost:${port}`);
});

const client = new mongodb.MongoClient("mongodb://localhost:27017", {
  useNewUrlParser: true,
});

let db;
client.connect((err) => {
  db = client.db("myLobbies");
  if (err) {
    console.log("Error connecting to MongoDB", err);
  } else {
    console.log("Connected to MongoDB");
  }
});

app.use(bodyParser.json());

app.post("/api/lobbies", async (req, res) => {
  try {
    const { pseudonym } = req.body;
    const lobby = await db.collection("lobbies").insertOne({ pseudonym });
    const lobbyId = lobby.insertedId;
    const lobbyLink = `http://localhost:3000/lobby/${lobbyId}`;
    console.log("yikes");
    res.status(201).json({ lobbyId, lobbyLink });
  } catch (err) {
    console.log(err);
    res.status(500).json({ message: "Error creating lobby" });
  }
});

app.get("/api/lobbies/:lobbyId", async (req, res) => {
  try {
    const { lobbyId } = req.params;
    console.log("lobbyId", lobbyId);

    const lobby = await db
      .collection("lobbies")
      .findOne({ _id: new mongodb.ObjectId(lobbyId) });
    console.log("lobby", "hello");
    if (!lobby) {
      return res.status(404).json({ message: "Lobby not found" });
    }
    res.status(200).json({ message: "Lobby found" });
  } catch (err) {
    console.log(err);
    res.status(500).json({ message: "Error finding lobby" });
  }
});

app.post("/api/lobbies/:lobbyId/join", async (req, res) => {
  try {
    const { lobbyId } = req.params;
    console.log("join lobbyId", lobbyId);
    const { pseudonym } = req.body;
    const lobby = await db
      .collection("lobbies")
      .findOneAndUpdate(
        { _id: new mongodb.ObjectId(lobbyId) },
        { $push: { players: { pseudonym } } },
        { returnOriginal: false }
      );
    if (!lobby.value) {
      return res.status(404).json({ message: "Lobby not found" });
    }
    const lobbyLink = `http://localhost:3000/lobby/${lobbyId}`;
    res.status(200).json({ message: "Player joined lobby", lobbyLink });
  } catch (err) {
    console.log(err);
    res.status(500).json({ message: "Error joining lobby" });
  }
});

app.get("/api/lobbies/:lobbyId/players", async (req, res) => {
  try {
    const { lobbyId } = req.params;
    const lobby = await db
      .collection("lobbies")
      .findOne({ _id: new mongodb.ObjectId(lobbyId) });
    if (!lobby) {
      return res.status(404).json({ message: "Lobby not found" });
    }
    const players = lobby.players;
    res.status(200).json({ players });
  } catch (err) {
    console.log(err);
    res.status(500).json({ message: "Error finding players" });
  }
});
